#include "image.h"
#include <stdio.h>
#include <stdlib.h>

void turn_image(struct image* oimg, struct image* iimg){
	struct pixel* src = iimg->pixeldata;
	struct pixel* dst = oimg->pixeldata;
	uint32_t cntx = 0;
	uint32_t cnty = 0;
	for(;;){
		*(dst + (cntx)*oimg->width + oimg->width - 1 -cnty) = *(src);
		src++;
		cntx++;
		if(cntx == iimg->width){
			cntx = 0;
			cnty++;
			if(cnty == iimg->height){
				break;
			}
		}
	}

}

void free_img(struct image* img){
	free(img->pixeldata);
	free(img);
}
