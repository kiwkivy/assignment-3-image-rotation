#ifndef __IMAGE_H__
#define __IMAGE_H__

#include <stdint.h>


struct pixel{
	uint8_t B;
	uint8_t G;
	uint8_t R;
}__attribute__((packed));

struct image{
	uint32_t width;
	uint32_t height;

	struct pixel* pixeldata;
};

void turn_image(struct image* oimg, struct image* iimg);

void free_img(struct image* img);

#endif
