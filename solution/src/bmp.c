#define _POSIX_C_SOURCE 200112L
#include "bmp.h"
#include <stdint.h>
#include <stdio.h>
#include <stdlib.h>
#include <sys/types.h>
#include <unistd.h>


struct image* bmp_to_img(FILE* fi){
	struct bmp_header hdr;

	fread(&hdr, 1, sizeof(struct bmp_header), fi);

	struct image* img = (struct image*)malloc(sizeof(struct image));

	img->height = hdr.biHeight;
	img->width = hdr.biWidth;

	uint32_t padding = 4*((img->width*3)%4 != 0) - (img->width*3)%4;
	fseek(fi, (int64_t)(hdr.bOffBits-sizeof(struct bmp_header)), SEEK_CUR);

	img->pixeldata = (struct pixel*)malloc(img->width*img->height*3);

	for(struct pixel* i = img->pixeldata; i < img->pixeldata+img->width*img->height; i+=img->width){
		fread(i, sizeof(struct pixel), img->width, fi);
		if(padding){
			fseek(fi, padding, SEEK_CUR);
		}
	}

	fseek(fi, 0, SEEK_SET);

	return img;
}


struct image* prepare_file_and_img(FILE* fi, FILE* fo){
	struct bmp_header hdr;
	fread(&hdr, 1, sizeof(struct bmp_header), fi);

	struct image* img = (struct image*)malloc(sizeof(struct image));

	img->width = hdr.biHeight;
	img->height = hdr.biWidth;

	uint32_t padding = 4*((img->width*3)%4 != 0) - (img->width*3)%4;

	fflush(fo);
	ftruncate(fo->_fileno, (int64_t)(img->height*padding+img->height*img->width*sizeof(struct pixel)+hdr.bOffBits));

	uint32_t tmp = hdr.biWidth;
	hdr.biWidth = hdr.biHeight;
	hdr.biHeight = tmp;

	hdr.biSizeImage = img->height*padding+img->height*img->width*sizeof(struct pixel);
	hdr.biXPelsPerMeter = 0;
	hdr.biYPelsPerMeter = 0;

	fwrite(&hdr, sizeof(struct bmp_header), 1, fo);

	img->pixeldata = (struct pixel*)malloc(img->width*img->height*sizeof(struct pixel));

	fseek(fo, 0, SEEK_SET);

	return img;

}

void img_to_bmp(struct image* img, FILE* f){
	struct bmp_header hdr;
	fread(&hdr, 1, sizeof(struct bmp_header), f);
	fseek(f, hdr.bOffBits, SEEK_SET);

	uint32_t padding = 4*((img->width*3)%4 != 0) - (img->width*3)%4;

	for(struct pixel* i = img->pixeldata; i < img->pixeldata+img->width*img->height; i+= img->width){
		fwrite(i, sizeof(struct pixel), img->width, f);
		if(padding){
			fseek(f, padding, SEEK_CUR);
		}
	}
	fseek(f, 0, SEEK_SET);

}
