#include "bmp.h"
#include "image.h"
#include <stdio.h>

int main(int argc, char** argv){
	if(argc != 3){
		return 1;
	}

	FILE* filei = fopen(argv[1], "rw+");
	FILE* fileo = fopen(argv[2], "w+");

	struct image* imgi = bmp_to_img(filei);
	struct image* imgo = prepare_file_and_img(filei, fileo);

	turn_image(imgo, imgi);

	img_to_bmp(imgo, fileo);

	free_img(imgi);
	free_img(imgo);

	return 0;
}
